package main;

import lexer.Lexer;
import lexer.Token;
import parser.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static void main(String[] args){

        if(args == null || args.length == 0) {
            System.out.println("Vykonání vzorového programu 1:");
            runProgram("res/program1.txt");
            System.out.println("----------------------------------------------------");
            System.out.println("Vykonání vzorového programu 2:");
            runProgram("res/program2.txt");
            System.out.println("----------------------------------------------------");
            System.out.println("Vykonání vzorového programu 3:");
            runProgram("res/program3.txt");
        }
        else {
            for (String arg : args){
                runProgram(arg);
            }
        }

    }

    private static void runProgram(String path){
        Lexer lexer = new Lexer();
        try {
            List<Token> tokens = lexer.parse(new String(Files.readAllBytes(Paths.get(path))));
            Parser parser = new Parser(tokens);
            Program program = parser.readProgram();
            program.execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
