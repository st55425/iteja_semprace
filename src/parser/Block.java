package parser;

import parser.containers.MyNumber;
import parser.containers.Procedure;
import parser.containers.Text;
import parser.statements.Statement;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Queue;

public class Block {
    private HashMap<String, MyNumber> numbers;
    private HashMap<String, Text> texts;
    private HashMap<String, Procedure> procedures;
    private Queue<Statement> statements;

    public Block() {
        this.numbers = new HashMap<>();
        this.texts = new HashMap<>();
        this.procedures = new HashMap<>();
        this.statements = new ArrayDeque<>();
    }

    public HashMap<String, MyNumber> getNumbers() {
        return numbers;
    }

    public HashMap<String, Text> getTexts() {
        return texts;
    }

    public HashMap<String, Procedure> getProcedures() {
        return procedures;
    }

    public Queue<Statement> getStatements() {
        return statements;
    }

    public void addNumbers(HashMap<String, MyNumber> vars) {
        this.numbers.putAll(vars);
    }

    public void addTexts(HashMap<String, Text> consts) {
        this.texts.putAll(consts);
    }

    public void addProcedures(HashMap<String, Procedure> procedures) {
        this.procedures.putAll(procedures);
    }
    public void addStatement(Statement statement) {
        this.statements.add(statement);
    }

}
