package parser.containers;

import parser.Block;

public class Procedure {
    private String ident;
    private Block block;

    public Procedure() {
    }

    public Procedure(String ident, Block block) {
        this.ident = ident;
        this.block = block;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }
}
