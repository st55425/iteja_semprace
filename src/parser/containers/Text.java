package parser.containers;

public class Text {
    private String ident;
    private String value;
    private boolean initialized;

    public Text(String ident) {
        this.ident = ident;
        initialized = false;
    }

    public String getIdent() {
        return ident;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        initialized = true;
    }

    public boolean isInitialized() {
        return initialized;
    }
}
