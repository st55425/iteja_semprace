package parser.containers;

public class MyNumber {
    private String ident;
    private int value;
    private boolean initialized;

    public MyNumber(String ident) {
        this.ident = ident;
        initialized = false;
    }

    public String getIdent() {
        return ident;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        initialized = true;
    }

    public boolean isInitialized() {
        return initialized;
    }
}
