package parser;

import lexer.Token;

public class BadTokenTypeException extends Exception{
    private Token token;
    private String expected;

    public BadTokenTypeException(Token token, String expected) {
        this.token = token;
        this.expected = expected;
    }

    @Override
    public String getMessage() {
        return  " Error(" + token.getLine() + ","+ token.getPosition() +  "): Bad token type was found was found; Expected Tokentype: "
                + expected + " Found Tokentype: " + token.getType() + " Value: " + token.getValue();
    }
}
