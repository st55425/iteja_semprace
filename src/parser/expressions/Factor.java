package parser.expressions;

import parser.Block;
import parser.executionExceptions.BadDataTypeException;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;

public abstract class Factor {
    boolean negative;

    public boolean isNegative() {
        return negative;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }

    public abstract int evaluate(Block context) throws IdentNotFoundException, NullVariableException, BadDataTypeException;
}
