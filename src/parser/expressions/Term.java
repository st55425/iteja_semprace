package parser.expressions;

import parser.Block;
import parser.executionExceptions.BadDataTypeException;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;

import java.util.ArrayList;
import java.util.List;

public class Term {

    protected List<Factor> factors;
    // true: multiply, false: divide
    protected List<Boolean> operators;
    private int line;

    public Term() {
        factors = new ArrayList<>();
        operators = new ArrayList<>();
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public List<Factor> getFactors() {
        return factors;
    }

    public void setFactors(List<Factor> factors) {
        this.factors = factors;
    }

    public List<Boolean> getOperators() {
        return operators;
    }

    public void setOperators(List<Boolean> operators) {
        this.operators = operators;
    }

    public void addFactor(Factor factor){
        factors.add(factor);
    }

    public void addOperator(boolean operator){
        operators.add(operator);
    }

    public int evaluate(Block context) throws IdentNotFoundException, NullVariableException, BadDataTypeException {
        int result = factors.get(0).evaluate(context);
        for (int i = 1; i < factors.size(); i++) {
            if (operators.get(i-1)){
                result *= factors.get(i).evaluate(context);
            }else {
                int divisor = factors.get(i).evaluate(context);
                if (divisor ==0){
                    throw new ArithmeticException("Divide by zero on line " + line);
                }
                result /= divisor;
            }
        }
        return result;
    }

}
