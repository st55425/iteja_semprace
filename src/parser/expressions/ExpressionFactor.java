package parser.expressions;

import parser.Block;
import parser.executionExceptions.BadDataTypeException;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;

public class ExpressionFactor extends Factor {

    private Expression expression;

    public ExpressionFactor() {
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public int evaluate(Block context) throws IdentNotFoundException, NullVariableException, BadDataTypeException {
        if (negative){
            return expression.evaluate(context)*-1;
        }
        return expression.evaluate(context);
    }
}
