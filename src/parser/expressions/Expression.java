package parser.expressions;

import parser.Block;
import parser.executionExceptions.BadDataTypeException;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;

import java.util.ArrayList;
import java.util.List;

public class Expression {
    private List<Term> terms;

    public Expression() {
        terms = new ArrayList<>();
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    public void addTerm(Term term) {
        terms.add(term);
    }

    public int evaluate(Block context) throws IdentNotFoundException, NullVariableException, BadDataTypeException {
        int result = 0;
        for (Term term:terms) {
            result += term.evaluate(context);
        }
        return result;
    }
}
