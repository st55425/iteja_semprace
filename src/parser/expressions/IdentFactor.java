package parser.expressions;

import parser.Block;
import parser.executionExceptions.BadDataTypeException;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;

public class IdentFactor extends Factor{

    private String ident;
    private int line;

    public IdentFactor() {
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    @Override
    public int evaluate(Block context) throws IdentNotFoundException, NullVariableException, BadDataTypeException {
        if (context.getNumbers().containsKey(ident)){
            if (!context.getNumbers().get(ident).isInitialized()){
                throw new NullVariableException(ident, line);
            }
            if (negative){
                return context.getNumbers().get(ident).getValue()*-1;
            }
            return context.getNumbers().get(ident).getValue();
        } else if(context.getTexts().containsKey(ident)){
            throw new BadDataTypeException(ident, line);
        }else {
            throw new IdentNotFoundException(ident, line);
        }
    }
}
