package parser.expressions;

import parser.Block;

public class NumberFactor extends Factor {

    private int number;

    public NumberFactor() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public int evaluate(Block context) {
        if (negative){
            return number*-1;
        }
        return number;
    }
}
