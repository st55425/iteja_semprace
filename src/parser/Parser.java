package parser;

import lexer.Token;
import lexer.TokenType;
import parser.conditions.*;
import parser.containers.MyNumber;
import parser.containers.Procedure;
import parser.containers.Text;
import parser.expressions.*;
import parser.statements.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class    Parser {
    private Stack<Token> tokens;

    public Parser(List<Token> tokens) {
        this.tokens = new Stack<>();
        Collections.reverse(tokens);
        this.tokens.addAll(tokens);
    }

    public Program readProgram() throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.BEGIN);
        return new Program(readBlock(null));
    }

    private Block readBlock(Block parent) throws BadTokenTypeException, NullTokenException {
        Block block = new Block();

        if (parent != null){
            block.addNumbers(parent.getNumbers());
            block.addTexts(parent.getTexts());
            block.addProcedures(parent.getProcedures());
        }

        if(tokens.peek().getType() == TokenType.NUMBERWORD){
            block.addNumbers(readNumbers());
        }
        if(tokens.peek().getType() == TokenType.TEXTWORD){
            block.addTexts(readTexts());
        }
        while(tokens.peek().getType() == TokenType.PROCEDURE){
            Procedure procedure = readProcedure(block);
            block.getProcedures().put(procedure.getIdent(), procedure);
        }

        while (tokens.peek().getType() != TokenType.RIGHTCURLYBRACKET && tokens.peek().getType() != TokenType.END)
        {
            block.addStatement(readStatement());
        }
        if (parent == null){
            popAndCheckType(TokenType.END);
        }else{
            popAndCheckType(TokenType.RIGHTCURLYBRACKET);
        }

        return block;
    }
    private HashMap<String, MyNumber> readNumbers() throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.NUMBERWORD);
        HashMap<String, MyNumber> numbers = new HashMap<>();

        while (true){
            peekAndCheckType(TokenType.IDENT);
            Token ident = tokens.pop();
            MyNumber number = new MyNumber(ident.getValue());


            if (tokens.peek().getType() == TokenType.ASSIGN){
                tokens.pop();
                peekAndCheckType(TokenType.NUMBER);
                number.setValue(Integer.parseInt(tokens.pop().getValue()));
            }
            numbers.put(ident.getValue(), number);
            Token delimiter = tokens.pop();
            if (delimiter.getType() == TokenType.EOS){
                return numbers;
            }else if (delimiter.getType() != TokenType.COMMA){
                throw new BadTokenTypeException(delimiter, "COMMA/EOS");
            }
        }
    }
    private HashMap<String, Text> readTexts() throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.TEXTWORD);
        HashMap<String, Text> texts = new HashMap<>();

        while (true){
            peekAndCheckType(TokenType.IDENT);
            Token ident = tokens.pop();
            Text text = new Text(ident.getValue());


            if (tokens.peek().getType() == TokenType.ASSIGN){
                tokens.pop();
                peekAndCheckType(TokenType.TEXT);
                text.setValue(tokens.pop().getValue());
            }
            texts.put(ident.getValue(), text);
            Token delimiter = tokens.pop();
            if (delimiter.getType() == TokenType.EOS){
                return texts;
            }else if (delimiter.getType() != TokenType.COMMA){
                throw new BadTokenTypeException(delimiter, "COMMA/EOS");
            }
        }
    }

    private Procedure readProcedure(Block parent) throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.PROCEDURE);
        Procedure procedure = new Procedure();

        peekAndCheckType(TokenType.IDENT);
        Token ident = tokens.pop();

        procedure.setIdent(ident.getValue());
        popAndCheckType(TokenType.LEFTCURLYBRACKET);
        procedure.setBlock(readBlock(parent));
        return procedure;
    }

    private Statement readStatement() throws BadTokenTypeException, NullTokenException {
        switch (tokens.peek().getType()){
            case IDENT:
                return readSetStatement();
            case EXEC:
                return readExecStatement();
            case READ:
                return readReadStatement();
            case WRITE:
                return readWriteStatement();
            case IF:
                return readIfStatement();
            case WHILE:
                return readWhileStatement();
            default:
                throw new BadTokenTypeException(tokens.peek(), "IDENT/EXEC/READ/WRITE/IF/WHILE");
        }
    }

    private Statement readSetStatement() throws BadTokenTypeException, NullTokenException {
        peekAndCheckType(TokenType.IDENT);
        Token ident = tokens.pop();
        SetStatement statement = new SetStatement();
        statement.setLine(ident.getLine());
        statement.setIdent(ident.getValue());
        popAndCheckType(TokenType.ASSIGN);
        if (tokens.peek().getType() == TokenType.TEXT){
            statement.setText(tokens.pop().getValue());
        }else{
            statement.setExpression(readExpression());
        }
        return statement;
    }
    private Statement readExecStatement() throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.EXEC);
        peekAndCheckType(TokenType.IDENT);
        Token ident = tokens.pop();
        ExecStatement statement = new ExecStatement();
        statement.setLine(ident.getLine());
        statement.setIdent(ident.getValue());
        return statement;
    }

    private Statement readReadStatement() throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.READ);
        peekAndCheckType(TokenType.IDENT);
        Token ident = tokens.pop();
        ReadStatement statement = new ReadStatement();
        statement.setLine(ident.getLine());
        statement.setIdent(ident.getValue());
        return statement;
    }

    private Statement readWriteStatement() throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.WRITE);
        WriteStatement statement = new WriteStatement();
        Token token = tokens.pop();
        if (token.getType().equals(TokenType.IDENT) || token.getType().equals(TokenType.TEXT)){
            statement.setToken(token);
        }
        else {
             throw new BadTokenTypeException(token, "IDENT/TEXT");
        }
        return statement;
    }

    private Statement readIfStatement() throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.IF);
        IfStatement statement = new IfStatement();
        statement.setCondition(readCondition());
        popAndCheckType(TokenType.LEFTCURLYBRACKET);
        while (tokens.peek().getType() != TokenType.RIGHTCURLYBRACKET){
            statement.addStatement(readStatement());
        }
        popAndCheckType(TokenType.RIGHTCURLYBRACKET);
        return statement;
    }

    private Statement readWhileStatement() throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.WHILE);
        WhileStatement statement = new WhileStatement();
        statement.setCondition(readCondition());
        popAndCheckType(TokenType.LEFTCURLYBRACKET);
        while (tokens.peek().getType() != TokenType.RIGHTCURLYBRACKET){
            statement.addStatement(readStatement());
        }
        popAndCheckType(TokenType.RIGHTCURLYBRACKET);
        return statement;
    }

    private Condition readCondition() throws BadTokenTypeException, NullTokenException {
        Expression expression = readExpression();
        if (tokens.peek().getType() == TokenType.LEFTCURLYBRACKET){
            UnaryCondition condition = new UnaryCondition();
            condition.setExpression(expression);
            return condition;
        }

        BinaryCondition condition;
        switch (tokens.peek().getType()){
            case EQUALS:
                condition = new EqualsCondition();
                break;
            case LESSER:
                condition = new LessCondition();
                break;
            case LESSEREQUALS:
                condition = new LessEqualsCondition();
                break;
            case GREATER:
                condition = new GreaterCondition();
                break;
            case GREATEREQUALS:
                condition = new GreaterEqualsCondition();
                break;
            default:
                throw new BadTokenTypeException(tokens.peek(), "EQUALS/HASH/SMALLER/SMALLEQUALS/BIGGER/BIGEQUALS/LEFTCURLYBRACKET");
        }
        tokens.pop();
        condition.setLeft(expression);
        condition.setRight(readExpression());
        return condition;
    }

    private Expression readExpression() throws BadTokenTypeException, NullTokenException {
        Expression expression = new Expression();
        expression.addTerm(readTerm());
        while (tokens.peek().getType() == TokenType.PLUS || tokens.peek().getType() == TokenType.MINUS){
            if (tokens.peek().getType() == TokenType.PLUS){
                tokens.pop();
            }
            expression.addTerm(readTerm());
        }
        return expression;
    }

    private Term readTerm() throws BadTokenTypeException, NullTokenException {
        Term term = new Term();
        term.addFactor(readFactor());
        while (tokens.peek().getType() == TokenType.MULTIPLY || tokens.peek().getType() == TokenType.DIVIDE){
            if (tokens.peek().getType() == TokenType.MULTIPLY){
                term.addOperator(true);
            }
            else if (tokens.peek().getType() == TokenType.DIVIDE){
                term.addOperator(false);
            }
            else {
                throw new BadTokenTypeException(tokens.peek(), "DIVIDE/MULTIPLY");
            }
            tokens.pop();
            term.addFactor(readFactor());
        }
        return term;
    }

    private Factor readFactor() throws BadTokenTypeException, NullTokenException {
        boolean negative = false;
        Factor factor;
        if (tokens.peek().getType() == TokenType.MINUS){
            tokens.pop();
            negative = true;
        }
        switch (tokens.peek().getType()){
            case IDENT:
                factor = readIdentFactor();
                break;
            case NUMBER:
                factor = readNumberFactor();
                break;
            case LEFTBRACKET:
                factor = readExpressionFactor();
                break;
            default: throw new BadTokenTypeException(tokens.peek(), "IDENT/NUMBER/LEFTBRACKET");
        }
        factor.setNegative(negative);
        return factor;
    }

    private Factor readIdentFactor() throws BadTokenTypeException, NullTokenException {
        peekAndCheckType(TokenType.IDENT);
        IdentFactor factor = new IdentFactor();
        factor.setLine(tokens.peek().getLine());
        factor.setIdent(tokens.pop().getValue());
        return factor;
    }

    private Factor readNumberFactor() throws BadTokenTypeException, NullTokenException {
        peekAndCheckType(TokenType.NUMBER);
        NumberFactor factor = new NumberFactor();
        factor.setNumber(Integer.parseInt(tokens.pop().getValue()));
        return factor;
    }

    private Factor readExpressionFactor() throws BadTokenTypeException, NullTokenException {
        popAndCheckType(TokenType.LEFTBRACKET);
        ExpressionFactor factor = new ExpressionFactor();
        factor.setExpression(readExpression());
        popAndCheckType(TokenType.RIGHTBRACKET);
        return factor;
    }

    private void popAndCheckType(TokenType type) throws BadTokenTypeException, NullTokenException {
        if (tokens.empty()){
            throw new NullTokenException();
        }
        Token token = tokens.pop();
        if(token.getType() != type){
            throw new BadTokenTypeException(token, type.toString());
        }
    }

    private void peekAndCheckType(TokenType type) throws BadTokenTypeException, NullTokenException {
        if (tokens.empty()){
            throw new NullTokenException();
        }
        Token token = tokens.peek();
        if(token.getType() != type){
            throw new BadTokenTypeException(token, type.toString());
        }
    }
}
