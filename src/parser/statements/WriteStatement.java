package parser.statements;

import lexer.Token;
import lexer.TokenType;
import parser.Block;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;

public class WriteStatement extends Statement {

    private Token token;

    public WriteStatement() {
    }


    public void setToken(Token token) {
        this.token = token;
    }

    @Override
    public void execute(Block context) throws IdentNotFoundException, NullVariableException {
        if(token.getType().equals(TokenType.IDENT)){
            String ident = token.getValue();
            if (context.getTexts().containsKey(ident)){
                if (!context.getTexts().get(ident).isInitialized()){
                    throw new NullVariableException(ident, line);
                }
                System.out.println(context.getTexts().get(ident).getValue());
            }else if (context.getNumbers().containsKey(ident)){
                if (!context.getNumbers().get(ident).isInitialized()){
                    throw new NullVariableException(ident, line);
                }
                System.out.println(context.getNumbers().get(ident).getValue());
            }else {
                throw new IdentNotFoundException(ident, line);
            }
        }else{
            System.out.println(token.getValue());
        }
    }
}
