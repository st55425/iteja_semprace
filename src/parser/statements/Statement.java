package parser.statements;

import parser.Block;
import parser.executionExceptions.*;

public abstract class Statement {

    protected int line;

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public abstract void execute(Block context) throws IdentNotFoundException, ReadVarException, NullVariableException, BadDataTypeException, ProcedureException;
}
