package parser.statements;

import parser.Block;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.ReadVarException;

import java.util.Scanner;

public class ReadStatement extends Statement {
    private String ident;

    public ReadStatement() {
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    @Override
    public void execute(Block context) throws IdentNotFoundException, ReadVarException {
        Scanner scanner = new Scanner(System.in);
        if (context.getNumbers().containsKey(ident)){
            try {
                context.getNumbers().get(ident).setValue(scanner.nextInt());
            }
            catch (Exception e){
                throw new ReadVarException(ident, line);
            }

        }else if (context.getTexts().containsKey(ident)){
            String text = scanner.nextLine().trim();
            for (char character: text.toCharArray()) {
                if (!Character.isLetterOrDigit(character)){
                    throw new ReadVarException(ident, line);
                }
            }
            String newText = "'" + text + "'";
            context.getTexts().get(ident).setValue(newText);
        }else {
            throw new IdentNotFoundException(ident, line);
        }
    }
}
