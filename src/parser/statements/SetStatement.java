package parser.statements;

import parser.Block;
import parser.executionExceptions.BadDataTypeException;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;
import parser.expressions.Expression;

public class SetStatement extends Statement {

    private String ident;
    private Expression expression;
    private String text;

    public SetStatement() {
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void execute(Block context) throws IdentNotFoundException, NullVariableException, BadDataTypeException {
        if (context.getNumbers().containsKey(ident)){
            if (expression != null){
                context.getNumbers().get(ident).setValue(expression.evaluate(context));
            }
            else {
                throw new BadDataTypeException(ident, line);
            }
        }else if (context.getTexts().containsKey(ident)){
            if (text != null){
                context.getTexts().get(ident).setValue(text);
            }
            else {
                throw new BadDataTypeException(ident, line);
            }
        }else {
            throw new IdentNotFoundException(ident, line);
        }

    }
}
