package parser.statements;

import parser.Block;
import parser.executionExceptions.*;
import parser.containers.Procedure;

public class ExecStatement extends Statement {
    private String ident;

    public ExecStatement() {
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    @Override
    public void execute(Block context) throws IdentNotFoundException, ProcedureException {
        if (context.getProcedures().containsKey(ident)){
            Procedure procedure = context.getProcedures().get(ident);
            procedure.getBlock().addProcedures(context.getProcedures());
            for (Statement statement : procedure.getBlock().getStatements()) {
                try {
                    statement.execute(procedure.getBlock());
                } catch (Exception e) {
                    throw new ProcedureException(e, ident, line);
                }
            }
        }
        else {
            throw new IdentNotFoundException(ident, line);
        }
    }
}
