package parser.statements;

import parser.Block;
import parser.executionExceptions.*;
import parser.conditions.Condition;

import java.util.ArrayList;
import java.util.List;

public class IfStatement extends Statement {

    private Condition condition;
    private List<Statement> statements;

    public IfStatement()
    {
        statements = new ArrayList<>();
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public void addStatement(Statement statement) {
        this.statements.add(statement);
    }

    @Override
    public void execute(Block context) throws IdentNotFoundException, ReadVarException, NullVariableException, BadDataTypeException, ProcedureException {
        if (condition.evaluate(context)){
            for (Statement statement : statements) {
                statement.execute(context);
            }
        }
    }
}
