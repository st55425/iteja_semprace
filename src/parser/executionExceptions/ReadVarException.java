package parser.executionExceptions;

public class ReadVarException extends Exception {

    private int line;
    private String ident;

    public ReadVarException(String ident, int line) {
        this.line = line;
        this.ident = ident;
    }

    @Override
    public String getMessage() {
        return "Exception " + this.getClass().getSimpleName() + ":  Exception at line " + line + ". Reading variable "
                + ident +" was not successful";
    }
}
