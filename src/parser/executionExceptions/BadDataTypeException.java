package parser.executionExceptions;

public class BadDataTypeException extends Exception {

    private int line;
    private String ident;

    public BadDataTypeException(String ident, int line) {
        this.line = line;
        this.ident = ident;
    }

    @Override
    public String getMessage() {
        return "Exception " + this.getClass().getSimpleName() + ":  Exception at line " + line +". Variable " + ident + " is incompatible in current context";
    }
}
