package parser.executionExceptions;

public class NullVariableException extends Exception {

    private int line;
    private String ident;

    public NullVariableException(String ident, int line) {
        this.line = line;
        this.ident = ident;
    }

    @Override
    public String getMessage() {
        return "Exception " + this.getClass().getSimpleName() + ":  Exception at line " + line + ". Trying to access variable "
                + ident +", which is not initialized";
    }
}