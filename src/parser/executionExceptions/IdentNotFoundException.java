package parser.executionExceptions;

public class IdentNotFoundException extends Exception {

    private int line;
    private String ident;

    public IdentNotFoundException(String ident, int line) {
        this.line = line;
        this.ident = ident;
    }

    @Override
    public String getMessage() {
        return "Exception " + this.getClass().getSimpleName() + ":  Exception at line " + line + " Identifier " +
                ident + " does not exist in current context";

    }
}
