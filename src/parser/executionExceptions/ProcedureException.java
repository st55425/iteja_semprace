package parser.executionExceptions;

public class ProcedureException extends Exception {

    String ident;
    String message;
    int line;

    public ProcedureException(Exception exception, String ident, int line) {
        this.ident = ident;
        this.line = line;
        if (exception instanceof ProcedureException){
            message = ((ProcedureException) exception).message;
        }else {
            message = exception.getMessage() + "\n";
        }
    }

    @Override
    public String getMessage() {
        message += "when executing procedure " + ident + " at line " + line + "\n";
        return message;
    }
}
