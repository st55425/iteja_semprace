package parser.conditions;

import parser.Block;
import parser.executionExceptions.BadDataTypeException;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;
import parser.expressions.Expression;

public class UnaryCondition extends Condition {

    Expression expression;

    public UnaryCondition() {
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public boolean evaluate(Block context) throws IdentNotFoundException, NullVariableException, BadDataTypeException {
        return expression.evaluate(context) > 0;
    }

}
