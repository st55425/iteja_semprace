package parser.conditions;

import parser.Block;
import parser.executionExceptions.BadDataTypeException;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;

public abstract class Condition {

    public abstract boolean evaluate(Block context) throws IdentNotFoundException, NullVariableException, BadDataTypeException;
}
