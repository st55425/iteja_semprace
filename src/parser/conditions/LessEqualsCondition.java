package parser.conditions;

import parser.Block;
import parser.executionExceptions.BadDataTypeException;
import parser.executionExceptions.IdentNotFoundException;
import parser.executionExceptions.NullVariableException;

public class LessEqualsCondition extends BinaryCondition {

    @Override
    public boolean evaluate(Block context) throws IdentNotFoundException, NullVariableException, BadDataTypeException {
        return left.evaluate(context) <= right.evaluate(context);
    }
}
