package parser.conditions;

import parser.expressions.Expression;

public abstract class BinaryCondition extends Condition {

    Expression left;
    Expression right;

    public BinaryCondition() {
    }

    public Expression getLeft() {
        return left;
    }

    public void setLeft(Expression left) {
        this.left = left;
    }

    public Expression getRight() {
        return right;
    }

    public void setRight(Expression right) {
        this.right = right;
    }

}
