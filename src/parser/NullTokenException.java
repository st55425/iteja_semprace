package parser;

public class NullTokenException extends Exception{

    @Override
    public String getMessage() {
        return " Error: No more tokens finded and program was not ended properly";
    }
}
