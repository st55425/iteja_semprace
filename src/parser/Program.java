package parser;

import parser.executionExceptions.*;
import parser.statements.Statement;

public class Program {
    Block block;

    public Program(Block block) {
        this.block = block;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public void execute(){
        for (Statement statement : block.getStatements()) {
            try {
                statement.execute(block);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return;
            }
        }
    }
}
