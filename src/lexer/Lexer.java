package lexer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Lexer {

    private static final HashMap<String, TokenType> ALL_TOKEN_TYPES = new HashMap<>();
    static {
        ALL_TOKEN_TYPES.put("+", TokenType.PLUS);
        ALL_TOKEN_TYPES.put("-", TokenType.MINUS);
        ALL_TOKEN_TYPES.put("*", TokenType.MULTIPLY);
        ALL_TOKEN_TYPES.put("/", TokenType.DIVIDE);
        ALL_TOKEN_TYPES.put("read", TokenType.READ);
        ALL_TOKEN_TYPES.put("write", TokenType.WRITE);
        ALL_TOKEN_TYPES.put(",", TokenType.COMMA);
        ALL_TOKEN_TYPES.put(";", TokenType.EOS);
        ALL_TOKEN_TYPES.put("=", TokenType.ASSIGN);
        ALL_TOKEN_TYPES.put("==", TokenType.EQUALS);
        ALL_TOKEN_TYPES.put("(", TokenType.LEFTBRACKET);
        ALL_TOKEN_TYPES.put(")", TokenType.RIGHTBRACKET);
        ALL_TOKEN_TYPES.put("while", TokenType.WHILE);
        ALL_TOKEN_TYPES.put("if", TokenType.IF);
        ALL_TOKEN_TYPES.put("end", TokenType.END);
        ALL_TOKEN_TYPES.put("begin", TokenType.BEGIN);
        ALL_TOKEN_TYPES.put("procedure", TokenType.PROCEDURE);
        ALL_TOKEN_TYPES.put("number", TokenType.NUMBERWORD);
        ALL_TOKEN_TYPES.put("text", TokenType.TEXTWORD);
        ALL_TOKEN_TYPES.put("exec", TokenType.EXEC);
        ALL_TOKEN_TYPES.put("<", TokenType.LESSER);
        ALL_TOKEN_TYPES.put("<=", TokenType.LESSEREQUALS);
        ALL_TOKEN_TYPES.put(">", TokenType.GREATER);
        ALL_TOKEN_TYPES.put(">=", TokenType.GREATEREQUALS);
        ALL_TOKEN_TYPES.put("{", TokenType.LEFTCURLYBRACKET);
        ALL_TOKEN_TYPES.put("}", TokenType.RIGHTCURLYBRACKET);
    }

    private List<Token> tokens;
    private List<Lexem> lexems;

    public Lexer() {
        tokens = new ArrayList<>();
        lexems = new ArrayList<>();
    }

    public List<Token> parse(String source) throws UndefinedCharacterException, UndefinedTokenException {
        createLexems(source);
        for (Lexem lexem : lexems) {
            tokens.add(lexemToToken(lexem));
        }
        return tokens;
    }
    public List<Token> getTokens() {
        return tokens;
    }

    private void createLexems(String source) throws UndefinedCharacterException {
        StringBuilder tmp = new StringBuilder();

        String[] rows = source.split("\n");
        for (int i = 0; i < rows.length; i++) {
            rows[i] = rows[i].trim();
            char[] rowCharacters = rows[i].toCharArray();
            for (int j = 0; j < rowCharacters.length; j++) {
                // = pridej do possible twocharacters a pak vždycky checkni jestli je na tom řádku další znak a když jo a ten znak je zase possible tak vytvoř dvou lexem, jinak jedno lexem
                if (rowCharacters[j] == '\''){
                    if (tmp.length() >0 && tmp.toString().charAt(0) == '\''){
                         tmp.append(rowCharacters[j]);
                        tmp = insertToLexems(tmp, i, j);
                    }
                    else{
                        insertToLexems(tmp, i, j);
                         tmp.append(rowCharacters[j]);
                    }
                }
                else if (tmp.length() >0 && tmp.toString().charAt(0) == '\''){
                    tmp.append(rowCharacters[j]);
                }
                else if (Character.isLetterOrDigit(rowCharacters[j])){
                    tmp.append(rowCharacters[j]);
                }
                else if (Character.isWhitespace(rowCharacters[j])){
                    tmp = insertToLexems(tmp, i, j);
                }
                else if (isOneCharacterLexem(rowCharacters[j])){
                    tmp = insertToLexems(tmp, i, j);
                    lexems.add(new Lexem(Character.toString(rowCharacters[j]), i, j));
                }
                else if (isPossibleTwoCharOperator(rowCharacters[j])){
                    tmp = insertToLexems(tmp, i, j);
                    tmp.append(rowCharacters[j]);
                    if (rowCharacters.length > j+1 && isPossibleTwoCharOperator(rowCharacters[j+1])){
                        tmp.append(rowCharacters[++j]);
                    }
                    tmp = insertToLexems(tmp, i, j);
                }
                else {
                    throw new UndefinedCharacterException(i, j);
                }
            }
            tmp = insertToLexems(tmp, i, rows[i].length()-1);
        }
    }

    private boolean isOneCharacterLexem(char character){
        return character == '+' || character == '-' || character == '*' || character == '/'  ||
                character == ';' || character == ',' || character == '(' || character == ')' ||
                character == '{' || character == '}';
    }

    private boolean isPossibleTwoCharOperator(char character){
        return character == '>' || character == '<' || character == '=';
    }

    private StringBuilder insertToLexems(StringBuilder lexem, int row, int position){
        if (lexem.length() != 0) {
            lexems.add(new Lexem(lexem.toString(), row, position-lexem.length()+1));
        }
        return new StringBuilder();
    }


    private Token lexemToToken(Lexem lexem) throws UndefinedTokenException {
        String tmp = lexem.getText().toLowerCase();
        if (ALL_TOKEN_TYPES.containsKey(tmp)){
            return new Token(ALL_TOKEN_TYPES.get(tmp), tmp, lexem.getLine(), lexem.getPosition());
        }
        else if (tmp.charAt(0) == '\'' && tmp.charAt(tmp.length()-1) == '\''){
            String[] parts = tmp.substring(1, tmp.length()-1).split(" ");
            boolean isText;
            for (String part: parts) {
                isText = hasOnlyNumberOrDigit(part.trim());
                if (!isText){
                    throw new UndefinedTokenException(lexem.getText(), lexem.getLine(), lexem.getPosition());
                }
            }
            return new Token(TokenType.TEXT, lexem.getText().substring(1, lexem.getText().length()-1), lexem.getLine(), lexem.getPosition());
        }
        else {
            try {
                Integer.parseInt(lexem.getText());
                return new Token(TokenType.NUMBER, lexem.getText(), lexem.getLine(), lexem.getPosition());
            } catch (NumberFormatException e) {
                if (!Character.isLetter(lexem.getText().charAt(0)) && !hasOnlyNumberOrDigit(lexem.getText())) {
                    throw new UndefinedTokenException(lexem.getText(), lexem.getLine(), lexem.getPosition());
                }
                return new Token(TokenType.IDENT, lexem.getText(), lexem.getLine(), lexem.getPosition());
            }
        }
    }

    private boolean hasOnlyNumberOrDigit(String s) {
        if (s == null){
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            if ((!Character.isLetterOrDigit(s.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

}
