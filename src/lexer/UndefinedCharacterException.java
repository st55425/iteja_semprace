package lexer;

public class UndefinedCharacterException extends Exception {

    private int line;
    private int positionInLine;

    public UndefinedCharacterException(int line) {
        this.line = line;
        positionInLine = -1;
    }

    public UndefinedCharacterException(int line, int positionInLine) {
        this.line = line;
        this.positionInLine = positionInLine;
    }

    @Override
    public String getMessage() {
        if (positionInLine < 0){
            return "Error(line " + line + "): Undefined character was found";
        }
        return " Error(" + line + ","+ positionInLine +  "): Undefined character was found";
    }
}