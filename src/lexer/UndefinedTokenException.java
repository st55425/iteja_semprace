package lexer;

public class UndefinedTokenException extends Exception {

    private String token;
    private int line;
    private int positionInLine;

    public UndefinedTokenException(String token) {
        this.token = token;
        line = -1;
    }

    public UndefinedTokenException(String token, int line, int positionInLine) {
        this.token = token;
        this.line = line;
        this.positionInLine = positionInLine;
    }

    @Override
    public String getMessage() {
        if (line < 1){
            return "Undefined token was found: " + token;
        }
        return " Error(" + line +", "+ positionInLine + "): Undefined token was found: " + token;
    }

}
