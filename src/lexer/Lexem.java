package lexer;

public class Lexem {
    private String text;
    private int line;
    private int position;

    public Lexem(String text, int line, int position) {
        this.text = text;
        this.line = line;
        this.position = position;
    }

    public String getText() {
        return text;
    }

    public int getLine() {
        return line;
    }

    public int getPosition() {
        return position;
    }
}
